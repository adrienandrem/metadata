%
%
%

Metadata
========

Library metadata extraction

Metadata extraction to produce statistics and suggest potential improvements


Sources
-------

* Documents


Rankings
--------

* most recent
* contains more terms
* biggest size


Statistics
----------

* time range
* library total size
* global term counts


Categories
----------

* contains "keywords" term with empty keywords metadata
* invalid or automatic title
* contains "abstract" term with empty abstract metadata
* heaviest without info (filesize/termcount)


# Formats

## JPEG Metadata

See [ExifTool documentation][doc]

* DocumentName
* ModifyDate
* Artist
* Copyright
* DateTimeOriginal
* CreateDate
* UserComment
* XPTitle
* XPComment
* XPAuthor
* XPKeywords
* XPSubject
 
[doc]: https://www.sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html


## 

|PDF     |JPEG  |PNG        |
|--------|------|-----------|
|Title   |      |           |
|Author  |Artist|Author     |
|Keywords|      |           |
|Abstract|      |Description|
