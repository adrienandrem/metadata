#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 09:26:11 2019

@author: <jfd> jfd@recoll.org
"""

from __future__ import print_function

import os
import re
import csv

import argparse

from datetime import datetime

import xapian
from recoll import recoll


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy import Boolean, Integer, String

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sqlalchemy.sql import text


DBURL = 'sqlite:///~/.cache/metadata/library.db'
CONF = "~/.recoll"

RANKING_LIMIT = 10

COMMAND_PATTERN = r'exiftool -Title="%s" -Author="%s" -overwrite_original_in_place %s'

PATTERN_DEFAULT_MSO = [r'^Microsoft Word', r'^PowerPoint Presentation']


Base = declarative_base()


def url2path(url):
    return re.sub(r'^[a-z]+://', '', url)


def containsBlanks(string):
    return re.match(r'\\s', string)


def command(path, title, author):
    p = path if not containsBlanks(path) else r'"%s"' % path
    return COMMAND_PATTERN % (title, author, p)


def validTitle(string):
    title = string.strip()
    valid = True

    if len(title) == 0:
        valid = False
    if re.match(r'.*_', title) and (not re.match(r'.*\s', title)):
        valid = False
    if re.match(r'.*\s', title) and (not re.match(r'.*[a-z]', title)):
        valid = False
    if any([re.match(pattern, title, re.I) for pattern in PATTERN_DEFAULT_MSO]):
        valid = False
    if re.match(r'.*\.(doc|docx|ppt|dvi|pdf)$', title, re.I):
        valid = False

    if re.match(r'doi:[0-9]+/[0-9]+', title, re.I):
        valid = False
    # if re.match(r'PII:\s', title, re.I):
    #     valid = False

    return valid


def validFilename(string):
    valid = True

    if re.match(r'.*\s', string):
        valid = False
    if re.match(r'.*\'', string):
        valid = False

    return valid


class Document(Base):
    """Class to gather document metadata"""

    __tablename__ = "document"

    pk = Column(Integer, primary_key=True)
    id = Column(Integer, unique=True)
    filesize = Column(Integer)
    modifitationTime = Column(Integer)
    termcount = Column(Integer)
    title = Column(String)
    author = Column(String)
    keywords = Column(String)
    filename = Column(String)
    url = Column(String)
    valid = Column(Boolean)
    checked = Column(Boolean)

    def __init__(self, database, document):
        xapianDocument = database.get_document(int(document.xdocid))

        self.id = document.xdocid
        self.filesize = document.dbytes
        self.modifitationTime = document.fmtime
        self.termcount = xapianDocument.termlist_count()
        self.title = document.title
        self.author = document.author
        self.keywords = document.keywords
        # self.date
        # self.year
        # self.abstract

        self.filename = document.filename
        self.url = document.url
        # self.location = document.url
        #                 + ('|' + document.ipath if document.ipath else '')

        self.valid = None
        self.checked = False

    _fields = ['id',
               'filesize',
               'modifitationTime',
               'termcount',
               'title',
               'author',
               'keywords',
               'url']

    def _asdict(self):
        return {
            'id': self.id,
            'filesize': self.filesize,
            'modifitationTime': datetime.fromtimestamp(int(self.modifitationTime)),
            'termcount': self.termcount,
            'title': self.title,
            'author': self.author,
            'keywords': self.keywords,
            'url': self.url
            }

    def titleValid(self):
        """Checks title validity

        checks if:
        - title is not empty or
        - is the creation software default value"""
        valid = True

        if not self.title:
            valid = False
        else:
            valid = validTitle(self.title)

        return valid

    def authorValid(self):
        """Checks if author is not empty"""
        valid = True

        if not self.title:
            valid = False

        return valid

    def isValid(self):
        """Checks if metadata fields are not empty or well formated"""
        return all([
            self.titleValid(),
            self.authorValid(),
            validFilename(self.filename)
            ])

    def checkValidity(self):
        self.valid = self.isValid()


class Report:

    def getInfo():
        pass

    def printFigures():
        pass


class Ranking:
    """Document ranking according to size or date, etc"""

    def documents():
        """Returns document ordered list"""
        pass


def update():
    """Update database"""

    xdbpath = os.path.join(CONF, "xapiandb")

    # Retrieve all documents with a file name ending in .pdf. Of course,
    # could be any recoll query
    query = "mime:application/pdf -dir:/data/doc/perso -dir:/data/doc/osm/asso-gf/admin"

    # Connect to the Xapian and recoll database interfaces
    xdb = xapian.Database(xdbpath)
    db = recoll.connect(confdir=CONF)

    # Execute the recoll query
    q = db.query()
    q.execute(query)

    # For each result document, fetch the term count from xapian, and
    # print the wanted data (file size, term count, file modification
    # time, and url|ipath)
    documents = []
    for doc in q:
        d = Document(xdb, doc)
        d.checkValidity()

        documents.append(d)

    # Save to CSV
    with open("/tmp/documents.csv", "w") as csvfile:
        writer = csv.DictWriter(csvfile, Document._fields, delimiter=';')
        writer.writeheader()

        for d in documents:
            try:
                if not d.isValid():
                    writer.writerow(d._asdict())
            except:
                continue

    # Save to database
    engine = create_engine(DBURL, echo=False)
    Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()

    for d in documents:
        session.add(d)
    session.commit()

    session.close()


def report():
    """Compute and display report metrics"""

    engine = create_engine(DBURL, echo=False)

    conn = engine.connect()

    # Documents
    qa = text("SELECT count(d.id) FROM document d")
    qv = text("SELECT count(d.id) FROM document d WHERE d.valid")

    total = conn.execute(qa).fetchall()[0][0]
    valid = conn.execute(qv).fetchall()[0][0]

    ratio = float(valid)/float(total)

    print('Report: validity: %s%% (of %s documents)' % (round(ratio*100., 1), total))

    # Size
    qa = text("SELECT sum(d.filesize) FROM document d")
    qv = text("SELECT sum(d.filesize) FROM document d WHERE d.valid")

    total = conn.execute(qa).fetchall()[0][0]
    valid = conn.execute(qv).fetchall()[0][0]

    ratio = float(valid)/float(total)

    print('Report: validity: %s%% (of %s MB)' % (round(ratio*100., 1), round(total/(1024*1024))))

    # Terms
    qa = text("SELECT sum(d.termcount) FROM document d")
    qv = text("SELECT sum(d.termcount) FROM document d WHERE d.valid")

    total = conn.execute(qa).fetchall()[0][0]
    valid = conn.execute(qv).fetchall()[0][0]

    ratio = float(valid)/float(total)

    print('Report: validity: %s%% (of %s terms)' % (round(ratio*100., 1), total))


def rank():
    engine = create_engine(DBURL, echo=False)

    conn = engine.connect()

    # Filesize
    qrs = text("SELECT d.* FROM document d WHERE NOT d.valid ORDER BY d.filesize DESC LIMIT %s" % RANKING_LIMIT)

    rows = conn.execute(qrs).fetchall()
    print(r'Filesize, File, Command')
    for i, row in enumerate(rows):
        filesize, url = row[2], row[9]
        print('%s, %s' % (filesize, url2path(url)))

    # Date
    qrd = text("SELECT d.* FROM document d WHERE NOT d.valid ORDER BY \"modifitationTime\" DESC LIMIT %s" % RANKING_LIMIT)

    rows = conn.execute(qrd).fetchall()
    print(r'Date, File, Command')
    for i, row in enumerate(rows):
        date, url = row[3], row[9]
        try:
            print('%s, %s' % (date, url2path(url)))
        except:
            continue

    # Termcount
    qrt = text("SELECT d.* FROM document d WHERE NOT d.valid ORDER BY d.termcount DESC LIMIT %s" % RANKING_LIMIT)

    rows = conn.execute(qrt).fetchall()
    print(r'Termcount, File, Command')
    for i, row in enumerate(rows):
        termcount, url = row[4], row[9]
        try:
            print('%s, %s' % (termcount, url2path(url)))
        except:
            continue


if __name__ == "__main__":

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('-f', '--filename', help="Enable filename check")
    PARSER.add_argument('-d', '--database', help="Library database file location")
    PARSER.add_argument('-l', '--limit', help="List item number limit", type=int)
    PARSER.add_argument('action')
    args = PARSER.parse_args()

    action = args.action

    if args.database:
        dbfile = args.database
        DBURL = 'sqlite:///%s' % os.path.expanduser(dbfile)

    if args.limit:
        RANKING_LIMIT = args.limit

    CONF = os.path.expanduser('~/.recoll')

    if action == 'update':
        update()
    elif action == 'report':
        report()
    elif action == 'rank':
        report()
        rank()
    else:
        print('Error: unknown action')
        print('Usage: metadata [update report rank]')
        exit(1)
