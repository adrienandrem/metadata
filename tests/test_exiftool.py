#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 13:53:08 2019

@author: aandre
"""

import os
import logging

from pytest import fixture, mark

from subprocess import PIPE, Popen


logging.basicConfig(level=os.environ.get("LOGLEVEL", "DEBUG"))


def parseExiftoolOutput(text):
    """Exiftool metadata output to dict"""
    data = dict()
    for line in text.splitlines():
        fields = line.split(':')
        name, value = fields[0].strip(), ':'.join(fields[1:]).strip()
        data[name] = value

    return data


def getMetadata(filename):
    """Retrieve metadata from file"""
    command = ['exiftool', filename]

    process = Popen(command, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()

    if err:
        logging.error('Failure executing command: %s', command)

    metadata = parseExiftoolOutput(out)

    return metadata


@fixture
def getFilePDF():
    """Builds a new PDF file"""
    infile = 'tests/doc.md'
    outfile = 'tests/doc.pdf'

    if os.path.isfile(outfile):
        os.remove(outfile)

    process = Popen(['pandoc', '-s', infile, '-o', outfile],
                    stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()


@fixture
def getFilePNG():
    """Builds a new PNG file"""
    infile = 'tests/schema.pbm'
    outfile = 'tests/schema.png'

    if os.path.isfile(outfile):
        os.remove(outfile)

    process = Popen(['convert', infile, outfile], stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()


# PDF

def test_pdf_title(getFilePDF):
    """PDF file author writing"""
    filename = 'tests/doc.pdf'
    option, author = 'Author', 'John Doe'
    command = ['exiftool', '-%s=%s' % (option, author),
               '-overwrite_original_in_place', filename]

    # Write metadata
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()

    # Read metadata
    metadata = getMetadata(filename)

    assert 'Author' in metadata

    retrievedAuthor = metadata['Author']

    assert retrievedAuthor == author


@mark.skip(reason="Not implemented")
def test_pdf_author():
    pass


@mark.skip(reason="Not implemented")
def test_pdf_keywords():
    pass


# PNG

def test_png_author(getFilePNG):
    """PNG file author writing"""
    filename = 'tests/schema.png'
    option, author = 'Author', 'John Doe'
    command = ['exiftool', '-%s=%s' % (option, author),
               '-overwrite_original_in_place', filename]

    # Write metadata
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()

    # Read metadata
    metadata = getMetadata(filename)

    assert 'Author' in metadata

    retrievedAuthor = metadata['Author']

    assert retrievedAuthor == author


def test_png_desc(getFilePNG):
    """PNG file description writing"""
    filename = 'tests/schema.png'
    option, description = 'Description', 'Beautiful sketch'
    command = ['exiftool', '-%s=%s' % (option, description),
               '-overwrite_original_in_place', filename]

    # Write metadata
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()

    # Read metadata
    metadata = getMetadata(filename)

    assert 'Description' in metadata

    retrievedDesc = metadata['Description']

    assert retrievedDesc == description


@mark.skip(reason="Not implemented")
def test_png_keywords(getFilePNG):
    pass


# JPEG

@mark.skip(reason="Not implemented")
def test_jpg_author():
    pass


@mark.skip(reason="Not implemented")
def test_jpg_desc():
    pass


@mark.skip(reason="Not implemented")
def test_jpg_keywords():
    pass
