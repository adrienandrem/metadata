#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 12:27:42 2019

@author: aandre
"""

from pytest import mark

from metadata import url2path
from metadata import validTitle, validFilename


def test_url2path_0():
    url = r'file:///home/me/Documents/infos.pdf'
    path = r'/home/me/Documents/infos.pdf'

    assert url2path(url) == path


def test_url2path_1():
    url = r'file:///home/me/Documents/infos.pdf'
    filename = r'infos.pdf'

    assert url2path(url) != filename


def test_valid_title_pdf_0():
    title = ''  # and None?

    assert not validTitle(title)


def test_valid_title_pdf_1():
    titles = ['a.doc', 'a.docx', 'a.dvi', 'a.pdf', 'a.ppt']

    for title in titles:
        assert not validTitle(title)


def test_valid_title_pdf_2():
    titles = ['abc_defg']

    for title in titles:
        assert not validTitle(title)


def test_valid_title_pdf_3():
    titles = ['PLEASE DO NOT YELL']

    for title in titles:
        assert not validTitle(title)


def test_valid_title_pdf_4():
    titles = ['Microsoft Word - Document', 'Microsoft Word - ',
              'PowerPoint Presentation']

    for title in titles:
        assert not validTitle(title)


def test_valid_title_pdf_5():
    titles = ['doi:11/2222']

    for title in titles:
        assert not validTitle(title)


@mark.skip(reason="Not implemented")
def test_valid_title_pdf_6():
    titles = ['PII: ']

    for title in titles:
        assert not validTitle(title)


def test_valid_filename_0():
    filenames = ["my document.pdf", "john's document.pdf"]

    for filename in filenames:
        assert not validFilename(filename)
