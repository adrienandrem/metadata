#!/bin/bash

BASE=~/.cache/metadata/library.db

clear ; recollindex && \
  rm -f $BASE && \
  python src/metadata.py -d $BASE update && \
  python src/metadata.py -d $BASE -l 40 rank
